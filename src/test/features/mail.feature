# language: en
@mail
Feature: LogIn in Mail

  Scenario Outline: LogIn in Mail
    Given Open GoogleChrome and go to site
    When user enters valid <username> and valid <password>
    Then user should be able to login successfully

    Examples:
      | username | password  |
      | test   | 12345 |