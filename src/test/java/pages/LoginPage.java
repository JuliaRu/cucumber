package pages;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

public class LoginPage {

   protected WebDriver driver;

    private By fieldUsername = By.id("mailbox:login");
    private By fieldPassword = By.id("mailbox:password");
    private By btnSubmit = By.id("mailbox:submit");
    private By btnLogOut = By.id("PH_logoutLink");

    public LoginPage(WebDriver driver){
        this.driver = driver;

        String title = driver.getTitle();
        Assert.assertTrue(title.equals("Mail.ru: почта, поиск в интернете, новости, игры"));
    }

    public void enterUsername(String username){
        driver.findElement(fieldUsername).sendKeys(username);
    }
    public void enterPassword(String password){
        driver.findElement(fieldPassword).sendKeys(password);
    }
    public void clickBtnSubmit() {
        driver.findElement(btnSubmit).click();
    }
    public void clickBtnLogOut() {
        driver.findElement(btnLogOut).click();
    }
    public void Sleep(int millis) {
        try {
            TimeUnit.MILLISECONDS.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void quit() {
        driver.quit();
    }
}
