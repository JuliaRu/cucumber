package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.LoginPage;

public class SmokeTest {
    WebDriver driver;
    LoginPage login;

    @Given("^Open GoogleChrome and go to site$")
    public void open_GoogleChrome_and_go_to_site() throws Throwable {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://mail.ru/");
    }

    @When("^user enters valid (.*) and valid (.*)$")
    public void user_enters_valid_username_and_valid_password(String username, String password) throws Throwable {
        login = new LoginPage(driver);

        login.enterUsername(username);
        login.clickBtnSubmit();
        login.enterPassword(password);
    }

    @Then("^user should be able to login successfully$")
    public void user_should_be_able_to_login_successfully() throws Throwable {

        login.clickBtnSubmit();
        login.Sleep(2000);
        login.clickBtnLogOut();
        login.quit();
    }

}
